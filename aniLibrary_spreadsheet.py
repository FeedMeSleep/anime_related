#!/usr/bin/env python
import csv
import os
import dotenv
import sys
from dataclasses import dataclass


@dataclass()
class Env:
    dotenv.load_dotenv()
    libraryDir = os.getenv(f'{sys.platform}-ani_lib')


@dataclass()
class Anime:
    name: str = None
    size: [int, float] = 0

def getAnimes():
    pass


def main():
    env = Env()
    animes = list()
    anime = Anime()
    main_sep_count = env.libraryDir.count(os.sep) + 2

    for path, dir, files in os.walk(env.libraryDir):
        if path == env.libraryDir:
            continue

        anime =
        if len(path.split(os.sep)) > main_sep_count:
            pass

    print(main_sep_count)


if __name__ == '__main__':
    main()