#!/usr/bin/env python
import re
import os
import sys
import requests
from threading import Thread


class Anime:
    def __init__(self, name):
        self.name = name
        self._apiURL = "https://graphql.anilist.co"
        self.response = ""

        query = """
        query ($search: String, $type: MediaType) {
            Media (search: $search, type: $type) {
                title {
                    romaji
                }
                episodes
                status
                type
                nextAiringEpisode {
                    episode
                }
                id
            }
        }
        """
        variables = {"search": self.name, "type": "ANIME"}

        try:
            self.response = requests.post(
                self._apiURL, json={"query": query, "variables": variables}
            ).json()["data"]["Media"]
        except requests.exceptions.ConnectionError as ex:
            self.response = ex.__name__

        if self.response is None:
            print(
                "This directory ({}) is not named after an Anime.".format(
                    os.path.split(os.getcwd())[-1]
                )
            )
            sys.exit()

    def status(self):
        if self.response["status"] == "RELEASING":
            return {"AiredEp": self.response["nextAiringEpisode"]["episode"] - 1}
        if self.response["status"] == "FINISHED":
            return {"FAiredEp": self.response["episodes"]}
        else:
            return False

    def episode(self, latest_ep: int = 0):
        if not latest_ep:
            return self.response["episodes"]
        return latest_ep


def ap(apli, itemli):
    _1, _2 = int(itemli[0]), int(itemli[1])
    if (_2 - _1) > 1:
        for _ in range(_1 + 1, _2):
            apli.append(_)
    for item in itemli:
        apli.append(int(item))


def threadMissingCheck(itemList, appendList):
    listFirstInt = itemList[0]
    listLastInt = itemList[-1]
    for _ in range(listFirstInt, listLastInt + 1):
        for item in itemList:
            if _ == item:
                break

            if item == listLastInt:
                appendList.append(_)


if __name__ == "__main__":
    files = []
    dup_files = []
    missing = []
    episodes_in_directory = []
    title = ""
    episode = ""
    ostype = sys.platform

    title = os.path.split(os.getcwd())[-1]

    for item in os.listdir():
        if os.path.isfile(item) and os.path.splitext(item)[-1] == ".mp4":
            files.append(item)

        if item.split("_")[-1] == "2.mp4":
            dup_files.append(item)

    if dup_files:
        for f in dup_files:
            if f == dup_files[-1]:
                print(f"{f} are duplicate files, want to delete them? (Y/N): ", end=" ")
            else:
                print(f",{f}")

        choice = input("")
        if choice.upper() == "Y":
            for f in dup_files:
                os.remove(os.path.join(os.getcwd(), f))
                print(f"{f} removed successfully.")
        print(":\n:\n:")

    for file in files:
        try:
            episodes = re.search("[0-9]+-[0-9]+", file).group().split("-")
            ap(episodes_in_directory, episodes)
        except AttributeError:
            try:
                episode = (
                    re.search("-_[0-9]+_", file)
                    .group()
                    .replace("-", "")
                    .replace("_", "")
                )
                episodes_in_directory.append(int(episode))
            except AttributeError:
                pass

    episodes_in_directory.sort()
    i = 1
    num = 0
    episode_num = 1
    anime = Anime(title)
    anime_status = anime.status()
    anime_episodes = 0
    if not anime_status:
        anime_episodes = anime.episode(episodes_in_directory[-1])
    elif isinstance(anime_status, dict):
        try:
            anime_episodes = anime_status["AiredEp"]
        except KeyError:
            anime_episodes = anime_status["FAiredEp"]
            anime_status = ""
    else:
        anime_episodes = anime.episode()

    listLen = len(episodes_in_directory) // 2
    epD = [episodes_in_directory[:listLen], episodes_in_directory[listLen:]]
    threads = [
        Thread(target=threadMissingCheck, args=[epD[0], missing]),
        Thread(target=threadMissingCheck, args=[epD[1], missing]),
    ]

    for thread in threads:
        thread.start()

    if anime.response:
        print(
            "AniList Anime Page: https://anilist.com/anime/{}".format(
                anime.response["id"]
            )
        )
    elif anime.response == "requests.exceptions.ConnectionError":
        print(
            "Script has an API feature, connect to the internet and retry if you want to check Latest/Total"
            " episodes of the Anime.\n"
        )

    if type(anime_status) is dict:
        print(
            "Total Episodes in Anime: {} (Latest Available Episode. Anime is still Ongoing)".format(
                anime_status["AiredEp"]
            )
        )
        print(f"Locally Available Episodes: {len(episodes_in_directory)}")
    elif anime_episodes:
        print(f"Total episodes in Anime: {anime_episodes}.")
        print(f"Locally Available Episodes: {len(episodes_in_directory)}")

    if missing or (anime_episodes > episodes_in_directory[-1]):
        print(f"You are missing these Episodes in your {title} directory: ", end=" ")
    else:
        print(f"There are no missing Episodes in your {title} directory.")
        sys.exit()

    if (anime_episodes > episodes_in_directory[-1]) and missing:
        for num in missing:
            print(
                f"{num} and {episodes_in_directory[-1]+1} to {anime_episodes}."
            ) if num == missing[-1] else print(num, end=", ")
    elif anime_episodes > episodes_in_directory[-1]:
        print(f"Episodes {episodes_in_directory[-1]+1} to {anime_episodes}.")
    elif missing:
        for num in missing:
            # prev_num += 1
            if num == missing[-1]:
                print(num)
            # elif prev_num == num:
            # check if prevNumber + 1 is equals to current num and add it into a list so we can see if
            # the whole list consists of 1 episode difference and print out list[0] to list[-1]
            else:
                print(num, end=", ")
