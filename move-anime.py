#!/usr/bin/env python
import os
import sys
from time import sleep
import dotenv
from plyer import notification
from dataclasses import dataclass


@dataclass(frozen=True)
class ENV:
    dotenv.load_dotenv()
    src_dir: str = os.getenv(f"{sys.platform}-source_dir")
    dst_dir: str = os.getenv(f"{sys.platform}-dest_dir")

    if not dst_dir and not src_dir:
        print("You need to set both Destination and Source directory in the .env file")
        sys.exit()
    elif not dst_dir:
        print(
            "You need to set the Destination directory in the .env file. (Where the Anime will be moved to)"
        )
        sys.exit()
    elif not src_dir:
        print(
            "You need to set the Source directory in the .env file. (Where the Anime is downloaded or Copied to)"
        )
        sys.exit()


def get_dstList(destination: str, directories_list: list, anime_list: list) -> None:
    for item in os.listdir(destination):
        if os.path.isdir(os.path.join(destination, item)):
            item = {"name": item, "dstDir": os.path.join(destination, item)}
            if item not in directories_list and item["name"] not in anime_list:
                directories_list.append(item)
                anime_list.append(item["name"])



def remove_animepahe(filename: str, fullPath: str) -> bool:
    if 'AnimePahe' in filename:
        new = filename.replace('AnimePahe_', '')
        try:
            os.replace(fullPath, fullPath.replace(filename, new))
        except PermissionError:
            sleep(10)
            os.replace(fullPath, fullPath.replace(filename, new))
        return True
    else:
        return False


def getAnimeTitle(animeFile: str) -> str:
    animeFile = animeFile.replace('AnimePahe_', '')
    if animeFile.count('_-_') == 2:
        return '_-_'.join(animeFile.split('_-_')[:2])
    if animeFile.count('_-_') == 1:
        return animeFile.split('_-_')[0]

def main():
    env = ENV()
    dirList = []
    aniList = []
    processed_items = []

    get_dstList(env.dst_dir, dirList, aniList)

    for item in os.listdir(env.src_dir):
        if item in processed_items:
            sleep(1)
            continue

        anime = os.path.splitext(item)[0]
        if (not anime.startswith('AnimePahe_')) or (anime.endswith('.fdmdownload')):
            continue

        oldAnimeFile = os.path.join(env.src_dir, item) # Anime File
        newAnimeFile = item.replace('AnimePahe_', '') # "AnimePahe_" removed anime file
        item = os.path.join(env.src_dir, item) # Full Path
        anime = getAnimeTitle(anime) # Anime Title

        if os.path.isfile(oldAnimeFile) and oldAnimeFile.endswith('.mp4'):
            if not remove_animepahe(os.path.split(oldAnimeFile)[-1], item):
                continue

            if anime.endswith('.'):
                anime = anime.replace(anime[-1], '')

            if anime not in aniList:
                os.mkdir(os.path.join(env.dst_dir, anime))
                dirList.append({
                    'name': anime,
                    'destination': os.path.join(env.dst_dir, anime)
                })
                aniList.append(anime)

            for aniDetails in dirList:
                if aniDetails['name'] == anime:
                    try:
                        os.replace(os.path.join(env.src_dir, newAnimeFile), os.path.join(env.dst_dir, anime+os.sep+newAnimeFile))
                    except OSError:
                        import shutil
                        shutil.move(os.path.join(env.src_dir, newAnimeFile), os.path.join(env.dst_dir, anime+os.sep+newAnimeFile))
                    notification.notify(title='File Moved',
                                message=f'{aniDetails["name"]} has been moved to {env.dst_dir}',
                                app_name=f'Python {sys.version.split()[0]}',
                                timeout=1)
                    break
        processed_items.append(oldAnimeFile)


if __name__ == "__main__":
    loop = True
    while loop:
        main()
        sleep(10)
