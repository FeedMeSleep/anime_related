# Anime Related Scripts i made.
1. mvani.py [instructions](#markdown-header-instructions-for-mvani)
   
      -    Moves anime from download directory to the anime's directory in another directory. **DIRECTORY** (need to improve the code on this. Someday day in the future i guess.)

2. cfme.py [instructions](#markdown-header-instructions-for-cfme)
   
      - Checks for missing Episodes in an anime's directory

3. anss.py [instructions coming later](#)


## Prerequisites
- Install [python](https://www.python.org/downloads/)
- Install [PowerShell Core](https://github.com/PowerShell/PowerShell/releases/latest) (pwsh) 
- Don't forget to install required modules `py -m pip install -r requirements.txt`
- Optional:
     - [Windows Terminal](https://github.com/Microsoft/Terminal/releases/latest) (Download .mixbundle and run it.)
     - Function for [PowerShell](#markdown-header-powershell-func-example)
     - Internet Connection for API feature.
  

I'll add more details to this readme file another day. 

## Instructions for cfme
- Clone or Download the repository or Download the script seperately
- Open pwsh
- cd Into the directory of the Anime you want to check missing episodes on
- run the script. 
  - either like `py D:/path/to/script/cfme.py` or using [function alias](#markdown-header-powershell-func-example)

#### **PowerShell Func Example**:

First open pwsh and open the `$profile` file in any editor you want. You can type `$profile` to check the directory where the file is in to open it using GUI.

```powershell
function CheckForMissingEpisode {
   py D:/path/to/script/cfme.py
}
New-Alias cfme CheckForMissingEpisode
```
> This creates a fuinction alias you can use whenever you open pwsh. Easier use.

- You can change the function's name to whatever you want.

- Don't forget to change `D:/path/to/script/`

- If you have pwsh open when you save the `$profile` file just type `. $profile` in that pwsh window and you can use it.

## Instructions for mvani
- Clone or Download the repostory or Download the script and .env_ file seperately
- Open any cli that can run python3 / py3
- Change the path strings inside .env_ (`w` for windows / `l` for linux) and rename the file to .env
- run the script (with pyw like so: `pyw mvani.py`, this runs the script in the background.)

You can make this run on os startup so that you don't have to bother running it manually everything you restart your pc/laptop

Follow these steps to set it up (winodws only for now):

1. Make a file named `startup.ps1`

2. type this in the file: `pyw D:/path/to/script/mvani.py` and save

3. Open run box (win+r) 

4. Type `shell:startup` and press enter

5. copy the `startup.ps1` file to that folder.